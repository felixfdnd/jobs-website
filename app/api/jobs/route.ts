import { NextResponse } from 'next/server';
import { connect } from '../../utils/database';

export const GET = async (req: Request, res: Response) => {
    try {
    const connection = await connect();
    const result = await connection.query('SELECT * FROM jobs');
    const jobs = result[0];
    
    return NextResponse.json({ message: "OK", jobs }, { status: 200 });
    } catch (error) {
        return NextResponse.json({ message: "Error", error }, { status: 500, })
    }
}