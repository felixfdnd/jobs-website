'use client'

import { useSearchParams } from 'next/navigation';
import Link from 'next/link'

export default function Page() {
  const searchParams = useSearchParams()
  const job_name = searchParams.get("job_name")
  const job_description = searchParams.get("job_description")
  const minimum_qualification = searchParams.get("minimum_qualification")
  const minimum_experience = searchParams.get("minimum_experience")
  const skills = searchParams.get("skills")
  const benefit = searchParams.get("benefit")
  const employment_type = searchParams.get("employment_type")
  const position_level = searchParams.get("position_level")

  return (
    <main className="bg-zinc-50 min-h-screen">
      <div className='bg-blue-100 px-5 py-3 lg:flex lg:justify-between lg:px-14 lg:py-6 xl:px-20'>
        <div className='mb-2'>
          <h1 className='text-2xl font-semibold text-blue-900'>{job_name}</h1>
          <p className='text-base font-semibold text-blue-900'>{position_level} | {employment_type}</p>
        </div>
        <div className='my-auto'>
          <button className="bg-blue-500 rounded-md text-sm font-bold text-slate-50 py-2 px-3">Apply</button>
        </div>
      </div>
      <div className='px-5 lg:px-14 lg:py-5 xl:px-20'>
        <div className='py-3'>
          <p className='text-base font-semibold text-blue-900'>Job Description</p>
          <p className='text-sm text-blue-900'>{job_description}</p>
        </div>
        <div className='py-3'>
          <p className='text-base font-semibold text-blue-900'>Minimum Qualification</p>
          <p className='text-sm text-blue-900'>{minimum_qualification}</p>
        </div>
        <div className='py-3'>
          <p className='text-base font-semibold text-blue-900'>Minimum Experience</p>
          <p className='text-sm text-blue-900'>{minimum_experience} year(s)</p>
        </div>
        <div className='py-3'>
          <p className='text-base font-semibold text-blue-900'>Skills</p>
          <p className='text-sm text-blue-900'>{skills}</p>
        </div>
        <div className='py-3'>
          <p className='text-base font-semibold text-blue-900'>Benefit</p>
          <p className='text-sm text-blue-900'>{benefit}</p>
        </div>
      </div>
      <div className='px-5 my-4 lg:px-14 lg:flex lg:justify-end xl:px-20'>
        <Link href='/' className="bg-red-500 text-md font-bold text-slate-50 mr-2 rounded-lg py-2 px-3">Back</Link>
      </div>
    </main>
  )
}
