'use client'

import { useState, useEffect } from 'react';
import Link from 'next/link';

export default function Home() {

  const [jobs, setJobs] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('/api/jobs');
        const data = await response.json();
        setJobs(data.jobs);
      } catch (error) {
        return NextResponse.json({ message: "Error", error }, { status: 500, })
      }
    };
  
    fetchData();
  }, []);

  return (
    <main className="bg-gradient-to-br from-sky-100 to-neutral-100 min-h-screen p-3 lg:flex">
      <div className=''>
        <h1 className='text-3xl font-bold text-blue-900 lg:text-5xl lg:mt-3'><span className='text-orange-600'>Join</span> Our Team</h1>
        <div className='my-6'>
          <p className='text-blue-900 mb-2 lg:mt-12'>Employment Type</p>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox1" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Contract</label>
          </div>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox2" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox2" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Internship</label>
          </div>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox3" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox3" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Full Time</label>
          </div>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox4" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox4" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Temporary</label>
          </div>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox5" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox5" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Part-Time</label>
          </div>
        </div>
        <div className='my-6 lg:my-14'>
          <p className='text-blue-900 mb-2'>Position Level</p>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox6" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox6" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">GEO/GM/Direktur/Manager Senior</label>
          </div>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox7" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox7" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Manager/Asisten Manager</label>
          </div>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox8" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox8" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Supervisor/Koordinator</label>
          </div>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox9" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox9" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Pegawai non-managemen & non-supervisor</label>
          </div>
          <div className="flex items-center m-2 lg:my-6">
            <input id="checkbox10" type="checkbox" value="" className="w-4 h-4 bg-gray-100 rounded focus:ring-blue-500"/>
            <label for="checkbox10" className="ml-2 text-sm font-semibold text-blue-900 dark:text-gray-300">Lulusan baru/Pengalaman kerja kurang dari 1 tahun</label>
          </div>
        </div>
      </div>
      <div className='lg:grid grid-cols-3'>
        {jobs.map((job) => (
          <div key={job.id} className="max-w-sm overflow-hidden shadow-lg bg-slate-50 rounded-2xl my-3 mx-2">
            <div className="p-6 h-full lg:flex lg:justify-between lg:flex-col">
              <div>
                <div className="font-bold text-2xl mb-2 text-blue-900">{job.job_name}</div>
                <p className="text-blue-700 text-base">
                  {job.job_description}
                </p>
              </div>
              <div className="pt-4 pb-0 flex justify-between">
                <Link href={`/details?id=${job.id}&job_name=${job.job_name}&job_description=${job.job_description}&minimum_qualification=${job.minimum_qualification}&minimum_experience=${job.minimum_experience}&skills=${job.skills}&benefit=${job.benefit}&employment_type=${job.employment_type}&position_level=${job.position_level}`} className="bg-transparent text-md font-bold text-blue-900 mr-2 my-auto">View Details</Link>
                <button className="bg-blue-500 rounded-md text-md font-bold text-slate-50 mr-2 py-2 px-3">Apply</button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </main>
  )
}
