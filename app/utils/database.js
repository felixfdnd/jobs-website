import mysql from 'mysql2/promise';

export async function connect() {
  try {
    const connection = await mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'xyz',
    });
    return connection;
  } catch (error) {
    return NextResponse.json({ message: "Error connecting to MySQL", error }, { status: 500, })
  }
}
