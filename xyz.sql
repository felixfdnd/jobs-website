CREATE DATABASE xyz;

USE xyz;

CREATE TABLE jobs (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    job_name VARCHAR(255),
    job_description VARCHAR(1000),
    minimum_qualification VARCHAR(255),
    minimum_experience INT,
    skills VARCHAR(500),
    benefit VARCHAR(500),
    employment_type VARCHAR(100),
    position_level VARCHAR(100)
);

INSERT INTO jobs (job_name, job_description, minimum_qualification, minimum_experience, skills, benefit, employment_type, position_level)
VALUES 
    ('Database Administrator', 'Menguasai ms sql, sql cluster, sql replication/ha, tuning query, function/store procedure', 'Pendidikan Terakhir D3/S1 jurusan terkait', 2, 'SQL Server, MY SQL', '-', 'Contract', 'Pegawai non-manajemen & non-supervisor'),
    ('Product Owner', 'Technical skill: Strong project management skills,...', 'Pendidikan Terakhir D3/S1 jurusan terkait', 3, 'Communication, Leadership, Decision-making, Analytic skills', 'Flexible working hours, Health insurance', 'Full-Time', 'Manager/Asisten Manager'),
    ('Fullstack Developer', 'Software Architect: Design software architecture ie, front...', 'Pendidikan Terakhir D3/S1 jurusan terkait', 2, 'JavaScript, HTML, CSS, MySQL', 'Flexible working hours, Health insurance', 'Internship', 'Lulusan baru/Pengalaman kerja kurang dari 1 tahun'),
    ('PHP Developer', '-', 'Pendidikan Terakhir D3/S1 jurusan terkait', 1, 'HTML, CSS, and PHP', 'Flexible working hours, Health insurance', 'Temporaray', 'Pegawai non-manajemen & non-supervisor'),
    ('React Native Developer', 'Experience in web development with JavaScript and React Native', 'Pendidikan Terakhir D3/S1 jurusan terkait', 1, 'JavaScript dan React Native', 'Flexible working hours, Health insurance', 'Full-time', 'Pegawai non-manajemen & non-supervisor'),
    ('Golang Developer', '-', 'Pendidikan Terakhir D3/S1 jurusan terkait', 2, 'Golang', 'Flexible working hours, Health insurance', 'Part-Time', 'Pegawai non-manajemen & non-supervisor'),
    ('iOS Developer', 'Responsibilities Work as part of cross functional,...', 'Pendidikan Terakhir D3/S1 jurusan terkait', 3, 'Swift and Xcode', 'Flexible working hours, Health insurance', 'Full-time', 'Supervisor/Koordinator'),
    ('Project Support', 'Malakukan tinjauan dan memberikan umpan balik atas pelaksanaan...', 'Pendidikan Terakhir D3/S1 jurusan terkait', 4, 'Communication', 'Health insurance', 'Full-Time', 'Supervisor/Koordinator'),
    ('UI/UX Designer', 'Scope of Work: Conduct testing of sample application...', 'Pendidikan Terakhir D3/S1 jurusan terkait', 2, 'Wireframe, design, prototype, Figma', 'Competitive salary, Health insurance', 'Contract', 'Pegawai non-manajemen & non-supervisor');
